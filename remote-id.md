# FAA Remote ID Requirement

Requires broadcast of unique identifier, lat, long, alt, velocity, GCS position OR takeoff location, and time.  This can be satisfied with built in transmission, or an aftermarket broadcast module.

Manufacturers were expected to have this integrated Sept 16, 2022, with a relaxed deadline to Dec 16, 2022.  Operational requirement is in place from Sept 16, 2023.

A list of current aircraft certified for Remote ID compliance is available here: https://uasdoc.faa.gov/listDocs

# Dronelab Platform Compliance

| Aircraft | Compliance Status |
| --- | --- |
| alert-m300 | needs fw update, waiting on ugcs compatibility |
| alert-altax | no, Freefly working on software update to enable |
| p4rtk | no |
| chei-mavic2pro | no |
| chei-spark | no |
| chei-p4p 1 | no |
| chei-m600 | no |
